# README #

This android app named 'ForzaFc-HomeAssignment' created by me "RamaMohan P",

as a Home Assignment in the selection process for the position of Senior Android Engineer at Forza Football.

**Developer:**		RamaMohan. P

### Who do I talk to? ###

* Ramamohan P
  Rama.mohan551@gmail.com

### Home Assignment - Problem Statement  ###
 
Fetch a list of Team objects from
https://s3-eu-west-1.amazonaws.com/forza-assignment/android/teams.json

Your objective is to create an implementation to fetch this list of Team objects, parse them and show that the whole chain works using unit tests.


### Solution ###

* Implemented solution as said in above problem description 
* Used MVVM framework, Dependency Injection, Android X libraries, retrofit, coroutines  and other popular libraries.
* Added UI unit tests using espresso,
* Added Unit Test cases for ViewMode implementation using Mockito 
* Added Unit Test cases for Repository implementations 
* Handled & verified with unit tests to perform various kinds of Connection Exceptions, Different response status codes


### Libraries Used ###
 
* Retrofit2  - for Fetching Data from API  & GSON Data Converter  
* Android X Lifecycles - For MVVM, Live Data, ViewModel, extensions
* Dagger   - For  Dependency Injection
* Mockito - For Mocking classes while UnitTesting
* Espresso - For Instrumentation UI Tests
* Truth - For Assertions 
* Other Android X libraries
