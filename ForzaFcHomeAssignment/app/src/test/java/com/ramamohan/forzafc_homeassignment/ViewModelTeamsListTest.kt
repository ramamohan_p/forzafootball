package com.ramamohan.forzafc_homeassignment

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.ramamohan.forzafc_homeassignment.api.*
import com.ramamohan.forzafc_homeassignment.model.Team
import com.ramamohan.forzafc_homeassignment.repository.RepositoryTeams
import com.ramamohan.forzafc_homeassignment.viewmodel.ViewModelTeamsList
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
@ExperimentalCoroutinesApi
class ViewModelTeamsListTest {

    private val ZERO: Int = 0

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    lateinit var viewModelTeamsList: ViewModelTeamsList

    private val testDispatcher = TestCoroutineDispatcher()


    @Mock
    lateinit var repositoryTeams: RepositoryTeams


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun fetchTeamsList_success_shouldReturnThreeTeams() =
        testDispatcher.runBlockingTest {

            val teamObjectsCount = 3

            val strTeamName = "test team"
            var testTeamList = ArrayList<Team>()
            for (index in 1..teamObjectsCount) {
                testTeamList.add(Team("$strTeamName $index", false, "Test Country"))
            }

            Mockito.`when`(repositoryTeams.getTeamsList())
                .thenReturn(ApiResult.Success(testTeamList))

            viewModelTeamsList = ViewModelTeamsList(repositoryTeams)

            val teamsLiveDataValue = viewModelTeamsList.fetchTeamsLiveData().value
            assertThat(teamsLiveDataValue).isNotNull()
            assertThat(teamsLiveDataValue?.size).isEqualTo(teamObjectsCount)

            val verifyTeamAtIndex = 1
            assertThat(teamsLiveDataValue?.get(verifyTeamAtIndex)?.name)
                .isEqualTo("$strTeamName ${verifyTeamAtIndex + 1}")

            // check if loading is completed
            assertThat(viewModelTeamsList.fetchLoadStatus()?.value).isFalse()

            // error should be null
            val apiError = viewModelTeamsList.fetchError().value
            assertThat(apiError).isNull()
        }

    @Test
    fun fetchTeamsList_responseCode404_shouldReturnErrorStatusCode404() =
        testDispatcher.runBlockingTest {
            val error404 = ApiResult.Error(API_STATUS_CODE_ENDPOINT_NOT_FOUND, "")
            Mockito.`when`(repositoryTeams.getTeamsList()).thenReturn(error404)

            viewModelTeamsList = ViewModelTeamsList(repositoryTeams)

            val apiError = viewModelTeamsList.fetchError().value
            assertThat(apiError).isNotNull()
            assertThat(apiError?.statusCode).isEqualTo(API_STATUS_CODE_ENDPOINT_NOT_FOUND)

            // check if loading is completed
            assertThat(viewModelTeamsList.fetchLoadStatus()?.value).isFalse()

            // teams count should be 0
            val teamsLiveDataValue = viewModelTeamsList.fetchTeamsLiveData().value
            assertThat(teamsLiveDataValue).isNotNull()
            assertThat(teamsLiveDataValue?.size).isEqualTo(ZERO)
        }

    @Test
    fun fetchTeamsList_responseCode500_shouldReturnErrorStatusCode500() =
        testDispatcher.runBlockingTest {
            // test with  500 (Internal Server Error)
            val error500 = ApiResult.Error(API_STATUS_CODE_INTERNAL_SERVER_ERROR, "")
            Mockito.`when`(repositoryTeams.getTeamsList()).thenReturn(error500)

            viewModelTeamsList = ViewModelTeamsList(repositoryTeams)

            val apiError = viewModelTeamsList.fetchError().value
            assertThat(apiError).isNotNull()
            assertThat(apiError?.statusCode).isEqualTo(API_STATUS_CODE_INTERNAL_SERVER_ERROR)

            // check if loading is completed
            assertThat(viewModelTeamsList.fetchLoadStatus()?.value).isFalse()

            // teams list should be ZERO
            val teamsLiveDataValue = viewModelTeamsList.fetchTeamsLiveData().value
            assertThat(teamsLiveDataValue).isNotNull()
            assertThat(teamsLiveDataValue?.size).isEqualTo(ZERO)
        }

    @Test
    fun fetchTeamsList_unknownHostException_shouldReturnErrorStatusCode102() =
        testDispatcher.runBlockingTest {
            val unknownHostException = ApiResult.Error(API_STATUS_CODE_UNKNOWN_HOST, "")
            Mockito.`when`(repositoryTeams.getTeamsList()).thenReturn(unknownHostException)

            viewModelTeamsList = ViewModelTeamsList(repositoryTeams)


            val apiError = viewModelTeamsList.fetchError().value
            assertThat(apiError).isNotNull()
            assertThat(apiError?.statusCode).isEqualTo(API_STATUS_CODE_UNKNOWN_HOST)

            // check if loading is completed
            assertThat(viewModelTeamsList.fetchLoadStatus()?.value).isFalse()

            // teams list should be ZERO
            val teamsLiveDataValue = viewModelTeamsList.fetchTeamsLiveData().value
            assertThat(teamsLiveDataValue).isNotNull()
            assertThat(teamsLiveDataValue?.size).isEqualTo(ZERO)
        }

    @Test
    fun fetchTeamsList_timeoutException_shouldReturnAsError() =
        testDispatcher.runBlockingTest {

            val timeoutError = ApiResult.Error(API_STATUS_CODE_TIMEOUT_ERROR, "")
            Mockito.`when`(repositoryTeams.getTeamsList()).thenReturn(timeoutError)

            viewModelTeamsList = ViewModelTeamsList(repositoryTeams)


            val apiError = viewModelTeamsList.fetchError().value
            assertThat(apiError).isNotNull()
            assertThat(apiError?.statusCode).isEqualTo(API_STATUS_CODE_TIMEOUT_ERROR)

            // check if loading is completed
            assertThat(viewModelTeamsList.fetchLoadStatus()?.value).isFalse()

            // teams list should be ZERO
            val teamsLiveDataValue = viewModelTeamsList.fetchTeamsLiveData().value
            assertThat(teamsLiveDataValue).isNotNull()
            assertThat(teamsLiveDataValue?.size).isEqualTo(ZERO)
        }

    @Test
    fun fetchTeamsList_anyUnknownError_shouldReturnAsError() =
        testDispatcher.runBlockingTest {
            val unknownHostException = ApiResult.Error(API_STATUS_CODE_UNKNOWN_ERROR, "")
            Mockito.`when`(repositoryTeams.getTeamsList()).thenReturn(unknownHostException)

            viewModelTeamsList = ViewModelTeamsList(repositoryTeams)


            val apiError = viewModelTeamsList.fetchError().value
            assertThat(apiError).isNotNull()
            assertThat(apiError?.statusCode).isEqualTo(API_STATUS_CODE_UNKNOWN_ERROR)

            // check if loading is completed
            assertThat(viewModelTeamsList.fetchLoadStatus()?.value).isFalse()

            // teams list should be ZERO
            val teamsLiveDataValue = viewModelTeamsList.fetchTeamsLiveData().value
            assertThat(teamsLiveDataValue).isNotNull()
            assertThat(teamsLiveDataValue?.size).isEqualTo(ZERO)
        }


}