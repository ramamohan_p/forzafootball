package com.ramamohan.forzafc_homeassignment

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.reflect.TypeToken
import com.google.common.truth.Truth.assertThat
import com.google.gson.Gson
import com.ramamohan.forzafc_homeassignment.api.*
import com.ramamohan.forzafc_homeassignment.model.Team
import com.ramamohan.forzafc_homeassignment.repository.TeamsRepositoryTasks
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.HttpException
import retrofit2.Response
import java.net.SocketTimeoutException
import java.net.UnknownHostException


/*
  This class contains test cases to validate
  implementation of Repository
  against different exceptions & successful responses
 */
@RunWith(MockitoJUnitRunner::class)
@ExperimentalCoroutinesApi
class RepositoryTeamsTest {

    @Mock
    lateinit var teamsApi: TeamsApi

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    private val strEmptyResponseBody = ""

    @Test
    fun requestTeamApi_success_shouldReturnTwoTeamsList() =
        runBlockingTest {
            val expectedTeamsCount = 2
            val strResponseWithTeams =
                "[{\"name\":\"mock team 1\",\"national\":false,\"country_name\":\"mock team country\"},{\"name\":\"mock team 2\",\"national\":false,\"country_name\":\"mock team country\"}]"
            val listOfTeams = Gson().fromJson(
                strResponseWithTeams, object :
                    TypeToken<List<Team?>?>() {}.type
            ) as List<Team>
            val mockSuccessResponse: Response<List<Team>>? = Response.success(listOfTeams)
            Mockito.`when`(teamsApi.fetchTeams()).thenReturn(mockSuccessResponse)

            val mockRepositoryTasks = TeamsRepositoryTasks(teamsApi)

            val teamsList = mockRepositoryTasks.getTeamsList()
            assertThat(teamsList).isNotNull()
            assertThat(teamsList.succeeded).isTrue()
            assertThat(teamsList is ApiResult.Error).isFalse()
            assertThat(teamsList is ApiResult.Success).isTrue()

            val arrTeamsList = (teamsList as ApiResult.Success).data
            assertThat(arrTeamsList).isNotNull()
            assertThat(arrTeamsList?.size).isEqualTo(expectedTeamsCount)

            val index0 = 0
            assertThat(arrTeamsList?.get(index0)?.name).isEqualTo("mock team 1")
        }


    @Test
    fun requestTeamApi_responseCode404_shouldHandleAsStatusCode404() =
        runBlockingTest {
            // test with  404 (API_STATUS_CODE_ENDPOINT_NOT_FOUND)
            val errorResponseBody =
                strEmptyResponseBody.toResponseBody("application/json".toMediaTypeOrNull())
            val mockResponse404 =
                Response.error<List<Team>>(API_STATUS_CODE_ENDPOINT_NOT_FOUND, errorResponseBody)

            Mockito.`when`(teamsApi.fetchTeams()).thenReturn(mockResponse404)

            val mockRepositoryTasks = TeamsRepositoryTasks(teamsApi)

            val teamsList = mockRepositoryTasks.getTeamsList()
            // this error/exception should be handled and return error with status code
            assertThat(teamsList).isNotNull()
            assertThat(teamsList is ApiResult.Error).isTrue()
            val error = (teamsList as ApiResult.Error)
            assertThat(error.statusCode).isEqualTo(API_STATUS_CODE_ENDPOINT_NOT_FOUND)
        }


    @Test
    fun requestTeamApi_responseCode500_shouldHandleAsStatusCode500() =
        runBlockingTest {
            val errorResponseBody =
                strEmptyResponseBody.toResponseBody("application/json".toMediaTypeOrNull())
            val mockResponse500 =
                Response.error<List<Team>>(API_STATUS_CODE_INTERNAL_SERVER_ERROR, errorResponseBody)

            Mockito.`when`(teamsApi.fetchTeams()).thenReturn(mockResponse500)

            val mockRepositoryTasks = TeamsRepositoryTasks(teamsApi)

            val teamsList = mockRepositoryTasks.getTeamsList()
            // this error/exception should be handled and return error with status code
            assertThat(teamsList).isNotNull()
            assertThat(teamsList is ApiResult.Error).isTrue()
            val error = (teamsList as ApiResult.Error)
            assertThat(error.statusCode).isEqualTo(API_STATUS_CODE_INTERNAL_SERVER_ERROR)
        }


    @Test
    fun requestTeamApi_unknownHost_shouldReturnAsUnknownHostError() =
        runBlockingTest {
            given(teamsApi.fetchTeams()).willAnswer {
                throw UnknownHostException("")
            }
            val mockRepositoryTasks = TeamsRepositoryTasks(teamsApi)

            val teamsList = mockRepositoryTasks.getTeamsList()
            // this error/exception should be handled and return error with status code
            assertThat(teamsList).isNotNull()
            assertThat(teamsList.succeeded).isFalse()
            assertThat(teamsList is ApiResult.Error).isTrue()
            val error = (teamsList as ApiResult.Error)
            assertThat(error.statusCode).isEqualTo(API_STATUS_CODE_UNKNOWN_HOST)
        }

    @Test
    fun requestTeamApi_timeoutException_shouldReturnAsTimeoutError() =
        runBlockingTest {
            given(teamsApi.fetchTeams()).willAnswer {
                throw SocketTimeoutException("")
            }
            val mockRepositoryTasks = TeamsRepositoryTasks(teamsApi)

            val teamsList = mockRepositoryTasks.getTeamsList()
            // this error/exception should be handled and return error with status code
            assertThat(teamsList).isNotNull()
            assertThat(teamsList.succeeded).isFalse()
            assertThat(teamsList is ApiResult.Error).isTrue()
            val error = (teamsList as ApiResult.Error)
            assertThat(error.statusCode).isEqualTo(API_STATUS_CODE_TIMEOUT_ERROR)
        }

    @Test
    fun requestTeamApi_anyOtherHttpException_shouldReturnAsError() =
        // all other unhandled exceptions
        runBlockingTest {
            val mockException: HttpException = mock(HttpException::class.java)
            Mockito.`when`(teamsApi.fetchTeams()).thenThrow(mockException)

            val mockRepositoryTasks = TeamsRepositoryTasks(teamsApi)

            val teamsList = mockRepositoryTasks.getTeamsList()
            // this error/exception should be handled and return error with status code
            assertThat(teamsList).isNotNull()
            assertThat(teamsList.succeeded).isFalse()
            assertThat(teamsList is ApiResult.Error).isTrue()
            val error = (teamsList as ApiResult.Error)
            assertThat(error.statusCode).isEqualTo(API_STATUS_CODE_UNKNOWN_ERROR)
        }


}