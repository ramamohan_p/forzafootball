package com.ramamohan.forzafc_homeassignment.repository

import com.ramamohan.forzafc_homeassignment.api.*
import com.ramamohan.forzafc_homeassignment.model.Team
import com.ramamohan.forzafc_homeassignment.utils.EspressoIdlingResource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class TeamsRepositoryTasks(teamsApi: TeamsApi) : RepositoryTeams {
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.Main
    var apiTeams: TeamsApi = teamsApi

    override suspend fun getTeamsList(): ApiResult<List<Team>?> {
        EspressoIdlingResource.increment() // Set app as busy.
        return  withContext(ioDispatcher) {
            try {
                val response = apiTeams.fetchTeams()
                EspressoIdlingResource.decrement() // Set app as idle.
                if (response.isSuccessful) {
                     return@withContext ApiResult.Success(response.body())
                } else {
                     return@withContext  ApiResult.Error(response.code(), response.message())
                }
            } catch (e: Exception) {
                if (e is UnknownHostException) {
                    return@withContext  ApiResult.Error(API_STATUS_CODE_UNKNOWN_HOST, "")
                }else if (e is SocketTimeoutException) {
                    return@withContext  ApiResult.Error(API_STATUS_CODE_TIMEOUT_ERROR, "")
                } else {
                    return@withContext  ApiResult.Error(
                        API_STATUS_CODE_UNKNOWN_ERROR,
                        e.message ?: ""
                    )
                }
            }
            return@withContext ApiResult.Error(API_STATUS_CODE_UNKNOWN_ERROR, ("Illegal state"))
        }
    }
}