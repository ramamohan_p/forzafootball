package com.ramamohan.forzafc_homeassignment.dependency

import com.ramamohan.forzafc_homeassignment.viewmodel.AppViewModelFactory
import com.ramamohan.forzafc_homeassignment.viewmodel.ViewModelTeamsList
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RetrofitModule::class])
interface AppComponent {
    fun inject(viewModelTeamsList: ViewModelTeamsList)
    fun inject(appViewModelFactory: AppViewModelFactory)
}