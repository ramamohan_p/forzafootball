package com.ramamohan.forzafc_homeassignment.model

data class Team(val name: String, val national: Boolean, val country_name: String)