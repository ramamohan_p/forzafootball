package com.ramamohan.forzafc_homeassignment.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ramamohan.forzafc_homeassignment.api.TeamsApi
import com.ramamohan.forzafc_homeassignment.dependency.DaggerAppComponent
import com.ramamohan.forzafc_homeassignment.repository.TeamsRepositoryTasks
import retrofit2.Retrofit
import javax.inject.Inject

class AppViewModelFactory :ViewModelProvider.Factory {
    @Inject
    lateinit var retrofit: Retrofit
    lateinit var teamsApi: TeamsApi

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        DaggerAppComponent.create().inject(this)
        teamsApi = retrofit.create(TeamsApi::class.java)
        val repository = TeamsRepositoryTasks(teamsApi)
        if (modelClass.isAssignableFrom(ViewModelTeamsList::class.java)) {
            return ViewModelTeamsList(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}