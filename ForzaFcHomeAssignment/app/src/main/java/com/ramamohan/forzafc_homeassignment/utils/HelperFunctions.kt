package com.ramamohan.forzafc_homeassignment.utils

import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import java.io.IOException


fun AppCompatActivity.replaceFragmentWithNoHistory(fragment: Fragment, frameId: Int) {
    supportFragmentManager.inTransactionWithoutHistory { replace(frameId, fragment) }
}

inline fun FragmentManager.inTransactionWithHistory(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction().func().addToBackStack("Added Fragment").commit()
}

inline fun FragmentManager.inTransactionWithoutHistory(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction().func().commit()
}


fun initToolBar(toolbar: Toolbar?, textView: TextView?, title: String?, activity: AppCompatActivity?) {
    toolbar?.apply {
        textView?.text = title
        activity?.setSupportActionBar(toolbar)
        activity?.let {
            activity.supportActionBar?.setDisplayHomeAsUpEnabled(false)
            activity.supportActionBar?.title = ""
        }
    }
}


val <T> T.exhaustive: T get() = this

@BindingAdapter("android:visibility")
fun setVisibility(view: View, value: Boolean) {
    view.visibility = if (value) View.VISIBLE else View.GONE
}

fun View.makeVisible() {
    this.visibility = View.VISIBLE
}

fun View.makeGone() {
    this.visibility = View.GONE
}
