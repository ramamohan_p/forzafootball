package com.ramamohan.forzafc_homeassignment.views

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ramamohan.forzafc_homeassignment.R
import com.ramamohan.forzafc_homeassignment.utils.replaceFragmentWithNoHistory


class ActivityTeamsList : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_teams_list)
        replaceFragmentWithNoHistory(FragmentTeamsList(), R.id.container_fragment)
    }
}