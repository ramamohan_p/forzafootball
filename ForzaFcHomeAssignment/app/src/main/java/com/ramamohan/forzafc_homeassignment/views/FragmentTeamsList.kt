package com.ramamohan.forzafc_homeassignment.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ramamohan.forzafc_homeassignment.R
import com.ramamohan.forzafc_homeassignment.api.*
import com.ramamohan.forzafc_homeassignment.model.Team
import com.ramamohan.forzafc_homeassignment.utils.initToolBar
import com.ramamohan.forzafc_homeassignment.utils.makeGone
import com.ramamohan.forzafc_homeassignment.utils.makeVisible
import com.ramamohan.forzafc_homeassignment.viewmodel.AppViewModelFactory
import com.ramamohan.forzafc_homeassignment.viewmodel.ViewModelTeamsList
import kotlinx.android.synthetic.main.fragment_teams_list.view.*
import kotlinx.android.synthetic.main.toolbar_layout.view.*

class FragmentTeamsList : Fragment(), LifecycleOwner {

    companion object {
        fun newInstance() = FragmentTeamsList()
    }

    lateinit var layoutTeams: View
    private val adapter = RecycleAdapterTeams()
    private lateinit var viewModelTeamsList: ViewModelTeamsList

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        layoutTeams = inflater.inflate(R.layout.fragment_teams_list, container, false)
        return layoutTeams
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        setupViews()
        observeViewModel()
    }

    private fun setupViews() {
        val toolbar = layoutTeams.toolBar as Toolbar
        val title = toolbar.toolbar_title
        initToolBar(toolbar, title, getString(R.string.screenTitle), activity as AppCompatActivity)
        layoutTeams.recycleViewTeams.apply {
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }
        layoutTeams.recycleViewTeams.adapter = adapter
    }

    private fun initViewModel() {
        var viewModelFactory = AppViewModelFactory()
        viewModelTeamsList =
            ViewModelProviders.of(this, viewModelFactory).get(ViewModelTeamsList::class.java)
    }

    private fun observeViewModel() {
        layoutTeams.textErrorMessage.makeGone()
        viewModelTeamsList.fetchLoadStatus()
            .observe(viewLifecycleOwner, Observer<Boolean> { loadingStatus ->
                layoutTeams.progressBar.visibility = if (loadingStatus) View.VISIBLE else View.GONE
            })
        viewModelTeamsList.fetchError()
            .observe(viewLifecycleOwner, Observer<ApiResult.Error> { apiErrorResponse ->
                handleApiErrorResponse(apiErrorResponse)
            })

        viewModelTeamsList.fetchTeamsLiveData().observe(viewLifecycleOwner,
            Observer<List<Team>> { teams ->
                layoutTeams.textErrorMessage.makeGone()
                adapter.setTeams(teams)
            }
        )
    }

    private fun handleApiErrorResponse(apiErrorResponse: ApiResult.Error?) {
        apiErrorResponse?.let {
            layoutTeams.textErrorMessage.makeVisible()
            when (it.statusCode) {
                API_STATUS_CODE_UNKNOWN_HOST -> {
                    layoutTeams.textErrorMessage.text =
                        getString(R.string.message_to_user_api_error_unknownhost)
                }
                API_STATUS_CODE_UNKNOWN_ERROR -> {
                    layoutTeams.textErrorMessage.text =
                        String.format(
                            getString(R.string.message_to_user_api_error_unknownerror),
                            it.errorMessage
                        )
                }
                API_STATUS_CODE_ENDPOINT_NOT_FOUND -> {
                    layoutTeams.textErrorMessage.text =
                        getString(R.string.message_to_user_api_error_endpointerror)
                }
                API_STATUS_CODE_INTERNAL_SERVER_ERROR -> {
                    layoutTeams.textErrorMessage.text =
                        getString(R.string.message_to_user_api_error_internalserver)
                }
                else -> {
                    // handle all other possible
                }
            }
        }
    }


}