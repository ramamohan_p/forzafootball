package com.ramamohan.forzafc_homeassignment.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ramamohan.forzafc_homeassignment.api.ApiResult
import com.ramamohan.forzafc_homeassignment.model.Team
import com.ramamohan.forzafc_homeassignment.repository.RepositoryTeams
import kotlinx.coroutines.launch

class ViewModelTeamsList(private val repositoryTeams: RepositoryTeams) :
    ViewModel() {
    private val _errorOnAPI = MutableLiveData<ApiResult.Error>()
    private var _loading: MutableLiveData<Boolean> = MutableLiveData()
    private val _teamsMutableLiveData: MutableLiveData<List<Team>> = MutableLiveData()

    fun fetchTeamsLiveData(): LiveData<List<Team>> = _teamsMutableLiveData
    fun fetchError(): LiveData<ApiResult.Error> = _errorOnAPI
    fun fetchLoadStatus(): LiveData<Boolean> = _loading

    init {
        _loading.value = true
        fetchFromRepo()
    }

    private fun fetchFromRepo() {
        viewModelScope.launch {
            val fetchTeamsListTask = repositoryTeams.getTeamsList()
            if (fetchTeamsListTask is ApiResult.Success) {
                _teamsMutableLiveData.postValue(fetchTeamsListTask.data)
                _loading.postValue(false)
            } else if (fetchTeamsListTask is ApiResult.Error) {
                _teamsMutableLiveData.postValue(emptyList())
                onError(fetchTeamsListTask.statusCode, fetchTeamsListTask.errorMessage)
            }
        }
    }

    private fun onError(statusCode: Int, message: String) {
        _errorOnAPI.postValue(ApiResult.Error(statusCode, message))
        _loading.postValue(false)
    }

    override fun onCleared() {
        super.onCleared()
    }
}