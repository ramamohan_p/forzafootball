package com.ramamohan.forzafc_homeassignment.repository

import com.ramamohan.forzafc_homeassignment.api.ApiResult
import com.ramamohan.forzafc_homeassignment.model.Team

interface RepositoryTeams {
    suspend fun getTeamsList(): ApiResult<List<Team>?>
}
