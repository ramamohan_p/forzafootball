package com.ramamohan.forzafc_homeassignment.api

// list all possible STATUS CODEs of API response
const val API_STATUS_CODE_UNKNOWN_HOST = 102
const val API_STATUS_CODE_UNKNOWN_ERROR = 104
const val API_STATUS_CODE_TIMEOUT_ERROR = 105
const val API_STATUS_CODE_SUCCESS = 200
const val API_STATUS_CODE_ACCESS_FORBIDDEN = 403
const val API_STATUS_CODE_ENDPOINT_NOT_FOUND = 404
const val API_STATUS_CODE_INTERNAL_SERVER_ERROR = 500


/**
 * A generic class that holds a value with its loading status.
 * @param <T>
 */
sealed class ApiResult<out R> {

    data class Success<out T>(val data: T) : ApiResult<T>()
    data class Error(val statusCode: Int, val errorMessage: String) : ApiResult<Nothing>()
    object Loading : ApiResult<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$statusCode]"
            Loading -> "Loading"
        }
    }
}

/**
 * `true` if [Result] is of type [Success] & holds non-null [Success.data].
 */
val ApiResult<*>.succeeded
    get() = this is ApiResult.Success && data != null