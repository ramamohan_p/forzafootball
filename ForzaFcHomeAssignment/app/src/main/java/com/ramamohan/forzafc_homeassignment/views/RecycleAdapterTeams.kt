package com.ramamohan.forzafc_homeassignment.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ramamohan.forzafc_homeassignment.databinding.RowTeamsListBinding
import com.ramamohan.forzafc_homeassignment.model.Team

class RecycleAdapterTeams : RecyclerView.Adapter<RecycleAdapterTeams.ViewHolderTeamRow>() {
    var arrTeams: List<Team> = ArrayList()

    class ViewHolderTeamRow(val binding: RowTeamsListBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderTeamRow {
        val binding = RowTeamsListBinding.inflate(LayoutInflater.from(parent.context))
        return ViewHolderTeamRow(binding)
    }

    override fun getItemCount() = arrTeams.size
    override fun onBindViewHolder(holder: ViewHolderTeamRow, position: Int) {
        holder.binding.team = arrTeams[position]
        holder.binding.executePendingBindings()
    }

    fun setTeams(teamsList: List<Team>) {
        this.arrTeams = teamsList
        notifyDataSetChanged()
    }
}