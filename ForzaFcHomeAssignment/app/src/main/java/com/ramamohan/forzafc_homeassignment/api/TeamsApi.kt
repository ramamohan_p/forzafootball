package com.ramamohan.forzafc_homeassignment.api

import com.ramamohan.forzafc_homeassignment.model.Team
import retrofit2.Response
import retrofit2.http.GET

interface TeamsApi {
    //https://s3-eu-west-1.amazonaws.com/forza-assignment/android/teams.json
    @GET("/forza-assignment/android/teams.json")
    suspend fun  fetchTeams(): Response<List<Team>>
}