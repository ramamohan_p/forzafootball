package com.ramamohan.forzafc_homeassignment


import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.ramamohan.forzafc_homeassignment.testutils.CustomAssertions.Companion.hasItemCount
import com.ramamohan.forzafc_homeassignment.testutils.DataBindingIdlingResource
import com.ramamohan.forzafc_homeassignment.testutils.monitorActivity
import com.ramamohan.forzafc_homeassignment.utils.EspressoIdlingResource
import com.ramamohan.forzafc_homeassignment.views.ActivityTeamsList
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.CoreMatchers.not
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Integration test for the Teams List Fragment.
 * This uses Espresso and DataBindingIdlingResourceRule to wait until data loaded
 *
 */
@RunWith(AndroidJUnit4::class)
@MediumTest
@ExperimentalCoroutinesApi
class FragmentTeamsListTest {

    private val ANY_TEAM_NAME: String = "Italy"
    private val EXPECTED_TOTAL_TEAMS_COUNT: Int = 4

    // An Idling Resource that waits for Data Binding to have no pending bindings
    private val dataBindingIdlingResource = DataBindingIdlingResource()


    private fun launchActivity() {
        val activityScenario = ActivityScenario.launch(ActivityTeamsList::class.java)
        dataBindingIdlingResource.monitorActivity(activityScenario)
    }

    /**
     * Idling resources tell Espresso that the app is idle or busy. This is needed when operations
     * are not scheduled in the main Looper (for example when executed on a different thread).
     */
    @Before
    fun registerIdlingResource() {
        IdlingRegistry.getInstance().register(EspressoIdlingResource.countingIdlingResource)
        IdlingRegistry.getInstance().register(dataBindingIdlingResource)
    }

    /**
     * Unregister your Idling Resource so it can be garbage collected and does not leak any memory.
     */
    @After
    fun unregisterIdlingResource() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.countingIdlingResource)
        IdlingRegistry.getInstance().unregister(dataBindingIdlingResource)
    }


    @Test
    fun displayTeamsListFragment_whenLaunched() {
        // Launch Teams List activity
        launchActivity()
        // THEN - Verify teams label & recycler view is displayed on screen
        onView(withText(R.string.teams)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.recycleViewTeams)).check(ViewAssertions.matches(isDisplayed()))
    }


    @Test
    fun fetchTeamsList_recyclerview_rowsCount() {
        /* because we fetching teams list from API upon launch automatically,
         IdleResource holds until IDLE event to indicate completion of UI update
         with teams recyclerview loaded with API response
         espresso won't let us do anything until the task finishes.
         */
        // Launch Teams List activity
        launchActivity()
        //  verify teams recycle view loaded & check recycle view items count
        onView(withId(R.id.recycleViewTeams))
            .check((hasItemCount(EXPECTED_TOTAL_TEAMS_COUNT)))
        // check if progress bar is hidden/not displayed
        onView(withId(R.id.progressBar)).check(ViewAssertions.matches(not(isDisplayed())))
    }

    @Test
    fun fetchTeamsList_recyclerview_teamExists() {
        // Launch Teams List activity
        launchActivity()
        // check if any random/definitive team name expected to there in API response
        onView(withText(ANY_TEAM_NAME)).check(ViewAssertions.matches(isDisplayed()))
    }


}